/*
 * nk_debug.h
 *
 *  Created on: Apr 12, 2021
 *      Author: nenad
 */

#ifndef GENERIC_NK_DEBUG_H_
#define GENERIC_NK_DEBUG_H_

#if defined(__cplusplus)
extern "C"
{
#endif

#define nk_assert(expr)     (void) 0

#if defined(__cplusplus)
}
#endif

#endif /* GENERIC_NK_DEBUG_H_ */
